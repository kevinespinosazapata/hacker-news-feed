# Hacker news feed!

  

![hacker news img](https://cdn.iconscout.com/icon/free/png-256/hacker-news-2-569388.png)

  

Hacker news feed is a web application that fetches algolia's api to bring most recent hacker news stories. The client side connects to a mongodb database which is updated and populated each hour with the algolia's api information

  ### Run application
There are two ways to run the application in your local.

1. Using docker compose.
- You first need to have docker and docker compose installed on your machine.
- Clone this repo and exec the docker-compose file:
```bash
$ git clone git@gitlab.com:kevinespinosazapata/hacker-news-feed.git
$ cd hacker-news-feed
$ docker-compose build
$ docker-compose up
```
2. Starting both server and client applications with <b>npm</b>
- You will need to install [Node.js](https://nodejs.org/)
- Clone the repo
- Open two terminals to start then client and server

_terminal 1_
```bash
$ git clone git@gitlab.com:kevinespinosazapata/hacker-news-feed.git
$ cd hacker-news-client
$ npm start # listents on localhost:80
```
_terminal 2_
```bash
$ git clone git@gitlab.com:kevinespinosazapata/hacker-news-feed.git
$ cd hacker-news-server
$ npm start # listents on localhost:4000

# It exposes two routes
# GET /stories
# PUT /stories/:id
```

The server exposes two routes
```bash
# on localhost:4000

GET /stories
PUT /stories/:id
```

## Technologies:


<img src="https://i.ibb.co/C2VRmjS/logos-Technologies.png">

- Nestjs
- React
- Docker
- Mongodb

## Application flow

```mermaid
graph LR
A((React App)) -- 1 --> B([API nestjs])
B -- 2 --> D(Algolia's api)
D -- 3 --> B
B -- 4 --> E([Mongodb])
E -- 5 --> B
B -- 6 --> A
```

## Author
* <b>Kevin Espinosa Zapata</b>  - Developer - [espinosakev24](https://github.com/espinosakev24)