import React from 'react';
import { Header } from "./components/header/Header";
import { Feed } from "./components/Feed/Feed";

function App() {
  return (
    <>
      <Header />
      <Feed />
    </>
  );
}

export default App;
